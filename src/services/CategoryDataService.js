import http from "../http-common";

class CategoryDataService {
    getAll() {
      return http.get("/plantCategory");
    }
  
    get(id) {
      return http.get(`/plantCategory/${id}`);
    }
  
    create(data) {
      return http.post("/plantCategory", data);
    }

    getArticles() {
      return http.get("/article");
    }
  
    update(id, data) {
      return http.put(`/plantCategory/${id}`, data);
    }
  
    delete(id) {
      return http.delete(`/plantCategory/${id}`);
    }

    // findByTitle(title) {
    //   return http.get(`/tutorials?title=${title}`);
    // }
  }
  
  export default new CategoryDataService();