import http from "../http-common";

class PlantDataService {
    getAll() {
      return http.get("/plant");
    }
  
    get(id) {
      return http.get(`/plant/${id}`);
    }
  
    create(data) {
      return http.post("/plant", data);
    }

    getCategories() {
      return http.get("/plantCategory")
    }

    getTypes() {
      return http.get("/plantType")
    }
  
    update(id, data) {
      return http.put(`/plant/${id}`, data);
    }
  
    delete(id) {
      return http.delete(`/plant/${id}`);
    }
  
    // findByTitle(title) {
    //   return http.get(`/tutorials?title=${title}`);
    // }
  }
  
  export default new PlantDataService();