import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
    // mode: "history",
    routes: [
        // Plant
        {
            path: "/",
            alias: "/plants",
            name: "plants",
            component: () => import("./components/PlantsList")
        },
        {
            path: "/plants/add",
            name: "add",
            component: () => import("./components/AddPlant")
        },
        {
            path: "/plants/:id",
            name: "plants-details",
            component: () => import("./components/Plant")
        },

        // Article
        {
            path: "/articles",
            name: "articles",
            component: () => import("./components/ArticlesList")
        },
        {
            path: "/articles/add",
            name: "add",
            component: () => import("./components/AddArticle")
        },
        {
            path: "/articles/:id",
            name: "articles-details",
            component: () => import("./components/Article")
        },

        // Category
        {
            path: "/categories",
            name: "categories",
            component: () => import("./components/CategoriesList")
        },
        {
            path: "/categories/add",
            name: "add",
            component: () => import("./components/AddCategory")
        },
        {
            path: "/categories/:id",
            name: "categories-details",
            component: () => import("./components/Category")
        },

        // Type
        {
            path: "/types",
            name: "types",
            component: () => import("./components/TypesList")
        },
        {
            path: "/types/add",
            name: "add",
            component: () => import("./components/AddType")
        },
        {
            path: "/types/:id",
            name: "types-details",
            component: () => import("./components/Type")
        },

        // Chat
        {
            path: "/chatbots",
            name: "chatbots",
            component: () => import("./components/ChatbotsList")
        },
    ]
});