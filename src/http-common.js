import axios from 'axios';

export default axios.create({
    // baseURL: "https://api-ferma.herokuapp.com/api",
    baseURL: "http://31.220.62.156:3000/api",
    headers: {
      "Content-type": "application/json",
      "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjE4NzE2ODg4fQ.9wHFg90uJJYB545-9F5wztQYylvSQW89yjb_066Z2oM"
    }
});