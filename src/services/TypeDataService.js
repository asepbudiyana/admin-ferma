import http from "../http-common";

class TypesDataService {
    getAll() {
      return http.get("/plantType");
    }
  
    get(id) {
      return http.get(`/plantType/${id}`);
    }
  
    create(data) {
      return http.post("/plantType", data);
    }
  
    update(id, data) {
      return http.put(`/plantType/${id}`, data);
    }
  
    delete(id) {
      return http.delete(`/plantType/${id}`);
    }

    // findByTitle(title) {
    //   return http.get(`/tutorials?title=${title}`);
    // }
  }
  
  export default new TypesDataService();